# dsml-cancer-classfication
# Breast Cancer Classification

## Downloading Dataset 
` mkdir datasets
  mkdir datasets\original `

Download the [Dataset](https://www.kaggle.com/datasets/paultimothymooney/breast-histopathology-images) 

![dataset-download](images/1.webp)

Unzip the dataset in the original directory. To observe the structure of this directory, we’ll use the tree command:

` cd breast-cancer-classification\breast-cancer-classification\datasets\original
  tree 
`

![tree](images/2.jpg)

We have a directory for each patient ID. And in each such directory, we have the 0 and 1 directories for images with benign and malignant content.

## config.py 

This holds some configuration we’ll need for building the dataset and training the model. You’ll find this in the cancernet directory.

`
   import os

   INPUT_DATASET = "datasets/original"

   BASE_PATH = "datasets/idc"
   TRAIN_PATH = os.path.sep.join([BASE_PATH, "training"])
   VAL_PATH = os.path.sep.join([BASE_PATH, "validation"])
   TEST_PATH = os.path.sep.join([BASE_PATH, "testing"])

   TRAIN_SPLIT = 0.8
   VAL_SPLIT = 0.1
`
Screenshot: 
![config.py](images/3.webp)

## build_dataset.py 
This will split our dataset into training, validation, and testing sets in the ratio mentioned above- 80% for training (of that, 10% for validation) and 20% for testing. With the ImageDataGenerator from Keras, we will extract batches of images to avoid making space for the entire dataset in memory at once.

` from cancernet import config
from imutils import paths
import random, shutil, os

originalPaths=list(paths.list_images(config.INPUT_DATASET))
random.seed(7)
random.shuffle(originalPaths)

index=int(len(originalPaths)*config.TRAIN_SPLIT)
trainPaths=originalPaths[:index]
testPaths=originalPaths[index:]

index=int(len(trainPaths)*config.VAL_SPLIT)
valPaths=trainPaths[:index]
trainPaths=trainPaths[index:]

datasets=[("training", trainPaths, config.TRAIN_PATH),
          ("validation", valPaths, config.VAL_PATH),
          ("testing", testPaths, config.TEST_PATH)
]

for (setType, originalPaths, basePath) in datasets:
        print(f'Building {setType} set')

        if not os.path.exists(basePath):
                print(f'Building directory {base_path}')
                os.makedirs(basePath)

        for path in originalPaths:
                file=path.split(os.path.sep)[-1]
                label=file[-5:-4]

                labelPath=os.path.sep.join([basePath,label])
                if not os.path.exists(labelPath):
                        print(f'Building directory {labelPath}')
                        os.makedirs(labelPath)

                newPath=os.path.sep.join([labelPath, file])
                shutil.copy2(inputPath, newPath)
`

![build_dataset.py](images/4.jpg)

##  Run the script build_dataset.py:

` py build_dataset.py `

![script](images/5.jpg)

## cancernet.py:
The network we’ll build will be a CNN (Convolutional Neural Network) and call it CancerNet. This network performs the following operations:

Use 3×3 CONV filters
Stack these filters on top of each other
Perform max-pooling
Use depthwise separable convolution (more efficient, takes up less memory)

`
  from keras.models import Sequential
  from keras.layers.normalization import BatchNormalization
  from keras.layers.convolutional import SeparableConv2D
  from keras.layers.convolutional import MaxPooling2D
  from keras.layers.core import Activation
  from keras.layers.core import Flatten
  from keras.layers.core import Dropout
  from keras.layers.core import Dense
  from keras import backend as K

 class CancerNet:
   @staticmethod
    def build(width,height,depth,classes):
     model=Sequential()
     shape=(height,width,depth)
     channelDim=-1

    if K.image_data_format()=="channels_first":
      shape=(depth,height,width)
      channelDim=1

    model.add(SeparableConv2D(32, (3,3), padding="same",input_shape=shape))
    model.add(Activation("relu"))
    model.add(BatchNormalization(axis=channelDim))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Dropout(0.25))

    model.add(SeparableConv2D(64, (3,3), padding="same"))
    model.add(Activation("relu"))
    model.add(BatchNormalization(axis=channelDim))
    model.add(SeparableConv2D(64, (3,3), padding="same"))
    model.add(Activation("relu"))
    model.add(BatchNormalization(axis=channelDim))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Dropout(0.25))

    model.add(SeparableConv2D(128, (3,3), padding="same"))
    model.add(Activation("relu"))
    model.add(BatchNormalization(axis=channelDim))
    model.add(SeparableConv2D(128, (3,3), padding="same"))
    model.add(Activation("relu"))
    model.add(BatchNormalization(axis=channelDim))
    model.add(SeparableConv2D(128, (3,3), padding="same"))
    model.add(Activation("relu"))
    model.add(BatchNormalization(axis=channelDim))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Dropout(0.25))

    model.add(Flatten())
    model.add(Dense(256))
    model.add(Activation("relu"))
    model.add(BatchNormalization())
    model.add(Dropout(0.5))

    model.add(Dense(classes))
    model.add(Activation("softmax"))

    return model

`

![keras](images/6.jpg)
![keras-2](images/7.webp)

We use the Sequential API to build CancerNet and SeparableConv2D to implement depthwise convolutions. The class CancerNet has a static method build that takes four parameters- width and height of the image, its depth (the number of color channels in each image), and the number of classes the network will predict between, which, for us, is 2 (0 and 1).

In this method, we initialize model and shape. When using channels_first, we update the shape and the channel dimension.

Now, we’ll define three DEPTHWISE_CONV => RELU => POOL layers; each with a higher stacking and a greater number of filters. The softmax classifier outputs prediction percentages for each class. In the end, we return the model.

## train_model.py:

This trains and evaluates our model. Here, we’ll import from keras, sklearn, cancernet, config, imutils, matplotlib, numpy, and os.



`
 import matplotlib
 matplotlib.use("Agg")

 from keras.preprocessing.image import ImageDataGenerator
 from keras.callbacks import LearningRateScheduler
 from keras.optimizers import Adagrad
 from keras.utils import np_utils
 from sklearn.metrics import classification_report
 from sklearn.metrics import confusion_matrix
 from cancernet.cancernet import CancerNet
 from cancernet import config
 from imutils import paths
 import matplotlib.pyplot as plt
 import numpy as np
 import os

 NUM_EPOCHS=40; INIT_LR=1e-2; BS=32

 trainPaths=list(paths.list_images(config.TRAIN_PATH))
 lenTrain=len(trainPaths)
 lenVal=len(list(paths.list_images(config.VAL_PATH)))
 lenTest=len(list(paths.list_images(config.TEST_PATH)))

 trainLabels=[int(p.split(os.path.sep)[-2]) for p in trainPaths]
 trainLabels=np_utils.to_categorical(trainLabels)
 classTotals=trainLabels.sum(axis=0)
 classWeight=classTotals.max()/classTotals

 trainAug = ImageDataGenerator(
   rescale=1/255.0,
   rotation_range=20,
   zoom_range=0.05,
   width_shift_range=0.1,
   height_shift_range=0.1,
   shear_range=0.05,
   horizontal_flip=True,
   vertical_flip=True,
   fill_mode="nearest")

 valAug=ImageDataGenerator(rescale=1 / 255.0)

 trainGen = trainAug.flow_from_directory(
   config.TRAIN_PATH,
   class_mode="categorical",
   target_size=(48,48),
   color_mode="rgb",
   shuffle=True,
   batch_size=BS)
 valGen = valAug.flow_from_directory(
   config.VAL_PATH,
   class_mode="categorical",
   target_size=(48,48),
   color_mode="rgb",
   shuffle=False,
   batch_size=BS)
 testGen = valAug.flow_from_directory(
   config.TEST_PATH,
   class_mode="categorical",
   target_size=(48,48),
   color_mode="rgb",
   shuffle=False,
   batch_size=BS)

 model=CancerNet.build(width=48,height=48,depth=3,classes=2)
 opt=Adagrad(lr=INIT_LR,decay=INIT_LR/NUM_EPOCHS)
 model.compile(loss="binary_crossentropy",optimizer=opt,metrics=["accuracy"])


 M=model.fit_generator(
   trainGen,
   steps_per_epoch=lenTrain//BS,
   validation_data=valGen,
   validation_steps=lenVal//BS,
   class_weight=classWeight,
   epochs=NUM_EPOCHS)

 print("Now evaluating the model")
 testGen.reset()
 pred_indices=model.predict_generator(testGen,steps=(lenTest//BS)+1)

 pred_indices=np.argmax(pred_indices,axis=1)

 print(classification_report(testGen.classes, pred_indices, target_names=testGen.class_indices.keys()))

 cm=confusion_matrix(testGen.classes,pred_indices)
 total=sum(sum(cm))
 accuracy=(cm[0,0]+cm[1,1])/total
 specificity=cm[1,1]/(cm[1,0]+cm[1,1])
 sensitivity=cm[0,0]/(cm[0,0]+cm[0,1])
 print(cm)
 print(f'Accuracy: {accuracy}')
 print(f'Specificity: {specificity}')
 print(f'Sensitivity: {sensitivity}')

 N = NUM_EPOCHS
 plt.style.use("ggplot")
 plt.figure()
 plt.plot(np.arange(0,N), M.history["loss"], label="train_loss")
 plt.plot(np.arange(0,N), M.history["val_loss"], label="val_loss")
 plt.plot(np.arange(0,N), M.history["acc"], label="train_acc")
 plt.plot(np.arange(0,N), M.history["val_acc"], label="val_acc")
 plt.title("Training Loss and Accuracy on the IDC Dataset")
 plt.xlabel("Epoch No.")
 plt.ylabel("Loss/Accuracy")
 plt.legend(loc="lower left")
 plt.savefig('plot.png')
`

![screenshot-1](images/8.webp) 
![screenshot-2](images/9.webp)
!

Result: 

![output-1](images/10.webp)
![output-2](images/11.webp)




